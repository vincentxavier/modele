def somme(a: int, b: int) -> int:
    """Renvoie la somme de deux entiers

    5 + 3 = 8
    >>> somme(5, 3)
    8

    0 + 5 = 5
    >>> somme(0, 5)
    5

    3 + 0 = 3
    >>> somme(3, 0)
    3

    """
    return a + b

def multiplication(a: int, b: int) -> int:
    """
    Renvoie le produit
    2x3 = 6
    >>> multiplication(2, 3)
    6
    """
    return a*b